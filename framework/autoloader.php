<?php
abstract class AutoLoader {
	public function __construct(){		
	}
	
	private static function get_dirs() {
		$directories = array();
		$directories[] = './framework/';
		$directories[] = './models/';
		$directories[] = './views/';
		$directories[] = './controllers/';		
		$directories[] = './helpers/';				
		
		return $directories;
	}

	public static function autoLoader($className) {
		
	    foreach(AutoLoader::get_dirs() as $directory)
	    {
			if(file_exists(strtolower($directory.$className.'.model.php')))
            {
                include_once strtolower(strtolower($directory.$className.'.model.php'));
            }
			
			if(file_exists(strtolower($directory.$className.'.php')))
            {
                include_once strtolower(strtolower($directory.$className.'.php'));
            }
			
			if(file_exists(strtolower($directory.$className.'.controller.php')))
            {
                include_once strtolower(strtolower($directory.$className.'.controller.php'));
            }
	    }
	}
}

spl_autoload_register("AutoLoader::autoLoader");

ini_set('display_errors', 1); 
error_reporting(E_ALL);
?>