<?php 
class View extends Base {
	public function __construct($model = null){	
		parent::__construct($model);
		
		$this->Layout = "./views/shared/layout";
	}
	
	public function Render($view, $model = null) {
		$return = "";
		
		if(file_exists($this->Layout.".html"))
		{			
			$return = $this->loadView($this->Layout);
			$return = str_replace('{header}', $this->loadView("./views/shared/header"), $return);
			
			$return = (file_exists("./views/$view.html"))
				? str_replace('{content}', $this->loadView("./views/$view", $model), $return)
				: str_replace('{content}', $this->loadView("./views/shared/viewnotfound"), $return);
				
			$return = $this->getPartialView($return);
			$return = str_replace('{footer}', $this->loadView("./views/shared/footer"), $return);
		}else
		{
			$return = $this->loadView("./views/shared/layoutnotfound");
		}
		
		return $return;
	}
	
	private function loadView($view, $model = null) {
		$strBuilder = new StringBuilder();
		
		if(file_exists($view.".html")){
			$template = file($view.".html");

			foreach ($template as $line) 
			{
				$strBuilder->Append($line);
			}
		}else {
			$strBuilder->Append($this->loadView("./views/shared/viewnotfound"));
		}
		
		$return = (isset($model)) 
			? $this->bindModel($strBuilder->toString(), $model) 
			: $strBuilder->toString();
		
		unset($strBuilder);
		unset($template);
				
		return $return;
	}
	
	private function getPartialMap($template){
		$start = (strpos($template, "<partial>") + strlen("<partial>"));
		$end = strpos($template, "</partial>");
		$partialMap = substr($template, $start, ($end - $start));
		
		return $partialMap;
	}
	
	private function getPatialFilePath ($template){
		$partialMap = $this->getPartialMap($template);
		
		return str_replace(":", "/", $partialMap);
	}
	
	private function getPartialView($template){
		$partialMap = $this->getPartialMap($template);
		$partialPath = $this->getPatialFilePath($template);		
		$loadedPartial = $this->loadView("./views/".$partialPath);
		
		$template = str_replace("<partial>$partialMap</partial>", $loadedPartial, $template);
		
		return $template;
	}
	
	private function bindModel($view, $model){
		if(isset($model)){
			$ref = new ReflectionClass($model);
			$props = $ref->getProperties();
			
			foreach($props as $prop) {
				$name = '{'.$prop->getName().'}';
				$value = $prop->getValue($model);				
				
				if(is_object($value)) {
					$subRef = new ReflectionClass($value);
					$subProps = $subRef->getProperties();
					
					foreach($subProps as $sub){						
						$subName = '{'.$prop->getName().'.'.$sub->getName().'}';
						$subValue = $sub->getValue($value);
												
						if(!is_object($subValue)){
							$view = str_replace($subName, $subValue, $view);
						}
					}
				} else {
					if(is_array($value)) {
						foreach(array_keys($value) as $key){
							$obj = new ReflectionClass($value[$key]);
							$ps = $obj->getProperties();
							
							foreach($ps as $p) {
								$n = "{".$prop->getName().".".$key.".".$p->getName()."}";
								$v = $p->getValue($value[$key]);

								$view = str_replace($n, $v, $view);
							}					
						}
					} else {
						$view = str_replace($name, $value, $view);
					}
				}
				
				//self::bindModel($view, $prop);
			}	
		}
	
		return $view;
	}
}
?>