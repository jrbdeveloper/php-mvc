<?php
class Controller extends Base {
	public function __construct($viewFolder){
		parent::__construct();
		
		$this->viewFolder = $viewFolder.'/';
	}
	
	public function View($view, $model = null){
		$v = new View($model);
		
		print $v->Render($this->viewFolder.$view, $model);
	}
}
?>