﻿class Control {
    constructor() { }

    Create(properties) {
        switch (properties.control) {
            case "input":
                return this.CreateControl(properties);
            case "form":
                return this.CreateForm(properties);
            default:
                return this.CreateContainer(properties);
        }
    }

    CreateForm(properties) {
        var element = document.createElement("form");

        this._assignAttributes(element, properties);

        return element;
    }

    CreateControl(properties) {
        var element = document.createElement(properties.control); //input element, text

        if (properties.type)
            element.setAttribute('type', properties.type);

        this._assignAttributes(element, properties);

        return element;
    }

    CreateContainer(properties) {
        var element = document.createElement(properties.type);
        this._assignAttributes(element, properties);
        element.removeAttribute("type");

        if (properties.value) {
            element.innerHTML = properties.value;
        }

        if (properties.text) {
            element.innerText = properties.text;
        }

        return element;
    }

    CreateEmptyOption() {
        return this.CreateContainer({
            control: "option",
            type: "option",
            value: "",
            text: ""
        });
    }

    GetControl(control) {
        return $(control).get(0);
    }

    _assignAttributes(element, properties) {
        for (var key in properties) {
            if (properties.hasOwnProperty(key)) {

                if (key.toString().indexOf("_") >= 0) {
                    key = key.replace("_", "-");
                }

                element.setAttribute(key, properties[key]);

                if (!properties.control) {
                    element.removeAttribute("text");
                    element.removeAttribute("value");
                } else {
                    element.removeAttribute("control");
                }
            }
        }

        if (properties.cssClass) {
            element.setAttribute("class", properties.cssClass);
            element.removeAttribute("cssclass");
        }
    }
}