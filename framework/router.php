<?php
final class Router 
{
	private $object = array();

	public function __construct($get, $post = null) {
		
		// Get the name of the controller to instantiate
		$this->object['c'] = isset($get['c']) 
			? ucfirst($get['c']) 
			: null;
		
		// Get the name of the action to call
		$this->object['a'] = isset($get['a']) 
			? $get['a'] 
			: null;
		
		// Get the value of the ID parameter
		$this->object['p'] = isset($get['p']) 
			? $get['p'] 
			: null;
			
		$this->object['data'] = $post;
	}
	
	public function __destruct() {
		// Destroy the object property array
		unset($this->object);
	}
	
	private function __clone() { 
		return false; 
	}
	
	public function Route() {
		$controller = empty($this->object['c']) ? 'Home' : $this->object['c'];
		$action = empty($this->object['a']) ? 'Index' : $this->object['a'];
		$get = $this->object['p'];
		$post = $this->object['data'];
				
		if(class_exists($controller) && method_exists($controller, $action)) {
			$this->object['class'][$controller] = new $controller();
		} else if (!class_exists($controller)) { 
			$controller = 'ErrorHandler';
			$action = 'Index';
			$get 	= 404;			
			
			$this->object['class'][$controller] = new $controller();
		} else if(!method_exists($controller, $action)) {
			$controller = 'ErrorHandler';
			$action = 'Index';
			$get 	= 404;			
			
			$this->object['class'][$controller] = new $controller();
		}
				
		$parameter = "";
		
		if(!empty($get)) {
			$parameter = $get;
		} 
		
		if (!empty($post)) {
			$parameter = $post;
		}
			
		//print_r($this->object);
		
		// Return the value from the action called on the paticular controller
		return $this->object['class'][$controller]->$action($parameter);
	}
	
	public function ObjectArray() {
		return $this->object['class'][$controller];
	}
}
?>