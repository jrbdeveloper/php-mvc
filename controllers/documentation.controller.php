<?php 
class Documentation extends Controller {
	public function __construct(){
		parent::__construct("documentation");
	}
	
	public function Index(){		
		$this->View("index");		
	}
	
	public function Models(){		
		$this->View("models");		
	}
	
	public function Views(){		
		$this->View("views");		
	}
	
	public function Controllers(){		
		$this->View("controllers");		
	}
	
	public function ModelBinding(){		
		$this->View("modelbinding");		
	}
	
	public function Routing(){		
		$this->View("routing");		
	}
}
?>