<?php
final class StringBuilder {
	private $arrValues;
	
	function __construct() {
		$this->arrValues = new ArrayObject();
	}
	
	function __destruct() {	
	}
	
	public function Append($strValue) 	{
		$this->arrValues->append($strValue);
	}
	
	public function toString() 	{
		$arr = new ArrayObject();
		
		foreach ($this->arrValues as $item)
		{
			if($item != "")
				$arr->append($item);
		}
		
		return implode(" ",(array)$arr);
	}
}
?>