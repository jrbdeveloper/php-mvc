<?php
class Base {
	private $Properties = array();

	public function __set($name, $value) {
		$this->Properties[$name] = $value;
	}

	public function __get($name) {
		return array_key_exists($name, $this->Properties) ? $this->Properties[$name] : null;
	}

	public function __construct($model = null) {
		if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get")){
			@date_default_timezone_set(@date_default_timezone_get());
		}		
	}
}
?>