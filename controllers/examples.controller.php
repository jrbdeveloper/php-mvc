<?php 
class Examples extends Controller {
	public function __construct(){
		parent::__construct("examples");
	}
	
	public function Index(){
		$person = new Person();
		$person->FirstName = "John";
		$person->LastName = "Doe";
				
		$person->Addresses['Home'] = new Address();
		$person->Addresses['Home']->Street = "213 Home St.";
		$person->Addresses['Home']->City = "Portland";
		$person->Addresses['Home']->State = "OR";
		$person->Addresses['Home']->Zip = "97060";
		
		$person->Addresses['Work'] = new Address();
		$person->Addresses['Work']->Street = "456 Work St.";
		$person->Addresses['Work']->City = "Portland";
		$person->Addresses['Work']->State = "OR";
		$person->Addresses['Work']->Zip = "97060";
		
		$person->Wife = new Person();
		$person->Wife->FirstName = "Jane";
		$person->Wife->LastName = "Doe";
		
		$this->View("index", $person);
	}
	
	public function Routing(){
		$this->View("routing");
	}
	
	public function Models(){
		$this->View("models");
	}
	
	public function ModelBinding(){	
		$this->View("modelbinding");
	}
	
	public function Views(){
		$this->View("views");
	}
	
	public function Controllers(){
		$this->View("controllers");
	}
	
	public function Form(){
		$this->View("form");
	}
	
	public function FormPost($model){	
		$person = new Person();
		$person->FirstName = $model['FirstName'];
		$person->LastName = $model['LastName'];
		$person->EmailAddress = $model['EmailAddress'];
		$person->Password = $model['Password'];
		
		$this->View("formpost", $person);
	}
}
?>