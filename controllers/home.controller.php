<?php 
class Home extends Controller {
	public function __construct(){
		parent::__construct("home");
	}
	
	public function Index(){		
		$this->View("index");		
	}
	
	public function About(){
		$this->View("about");		
	}
	
	public function Contact(){
		$this->View("contact");		
	}
}
?>