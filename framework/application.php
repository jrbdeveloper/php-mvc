<?php 
class Application extends Base {
	public function __construct($start = null){	
		parent::__construct();
		
		$this->start = $start;
	}
	
	public function Start(){	
		$objRouter = new Router($_GET, $_POST);
		$objRouter->Route();
	}
}
?>