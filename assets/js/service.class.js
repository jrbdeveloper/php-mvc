﻿class ServiceHelper {
    constructor(base, container, spinner) {
        this.basePath = base;
        this.container = container;
        this.spinner = spinner;
    }

    get BasePath() {
        if (this.basePath.length === 0) return undefined;
        return this.basePath;
    }

    set BasePath(path) {
        this.basePath = path;
    }

    Request(id, api, async, method) {
        return this._get({ 'id': id }, api, async, method);
    }

    GetModel(id, api, async) {
        return this._get({ 'id': id }, api, async, "GET");
    }

    GetByModel(model, api, async) {
        return this._post(model, api, async);
    }

    Get(api) {
        return this._get(api);
    }

    Post(data, api, async) {
        return this._post(data, api, async);
    }

    GetList(data, api, async) {
        return this._get(data, api, async, "POST");
    }

    GetModelWithCallback(id, api, async, callback) {
        return this._getWithCallback({ id: id }, api, async, callback);
    }

    _get(api) {
        var self = this;
        var result;

        $.ajax({
            url: self.basePath + api,
            context: document.body,
            async: false
        }).done(function (data) {
            result = data;
            self.spinner.hide();
            self.container.show();
        });

        return result;
    }

    _post(data, api, async) {
        var ret;

        $.ajax({
            method: "POST",
            url: api,
            dataType: "text",
            //contentType: "application/json",
            data: data,
            async: async,
        }).done(function (result) {
            ret = result;
        }).fail(function (obj, msg) {
            console.log("Error requesting data from service.", JSON.stringify(obj) + ' ' + msg);
        });

        return ret;
    }

    _getWithCallback(data, api, async, callback) {
        $.ajax({
            method: "GET",
            url: api,
            dataType: "json",
            data: data,
            async: async,
        }).done(function (result) {
            callback(result);
        }).fail(function (obj, msg) {
            console.log("Error requesting data from service.", JSON.stringify(obj) + ' ' + msg);
        });
    }
}