# README #

Below are the basic instructions for getting the framework set up and how to constribute if you so choose to. Currently the framework hasn't incorporated any database connectivity. This could be an area you contribute code to.

### What is this repository for? ###

* This repository contains everything needed to make use of the PHP-MVC framework.
* Current Version: v0.0.5

### How do I get set up? ###

* Get the code - Clone the repository
* Dependencies - Depending on when you clone the repo you may need/want to update the library dependencies.
   * Dependencies
      * jQuery
      * jQuery Validation
      * bootstrap
      * prism
* Deployment instructions
   * Once you have incorporated your changes to the code, simply copy the files to your server that is configured to host PHP applications.
   * You could also incorporate it into your CI/CD pipelines using tools such as Jenkins or DevOps.

### Contribution guidelines ###

* In order to contribute code, please clone the repo, branch from master and create a pull request.
* When creating the pull request, please include a short description of the change or enhancement.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact